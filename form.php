<?php

/**
* セッション開始
*/
session_start();


/**
* 外部ファイル読み込み
*/
include("config.php");
require_once("libs/common.inc.php");
require_once("vender/smarty/Smarty.class.php");


/**
* 変数の初期化
*/
$d 				= array();
$msg 			= array();
$msg['err'] 	= "";
$default_form 	= array(
		    "name" => "",
		    "email" => "",
		    "tel" => "",
		    "message" => ""
);


/**
* smarty初期化
*/
$smarty = new Smarty();

$smarty->template_dir = $smarty_templete_dir;
$smarty->compile_dir = $smarty_compile_dir;


/**
* submit処理
*/
if(isset($_POST['submit']))
{
	$order = key($_POST['submit']);
}
else
{
	$order = null;
}


/**
 * 確認画面へを押した時
 */
if($order === "next")
{
	$d = htmlspecailchars_array($_POST);
	$d = mb_convert_kana_array($d);

	/**
	* 入力チェック
	*/

	//名前　入力必須
	if($d['name'] === "")
	{
		$msg['err'] .= "名前が入力されていません<br>";
	}

	//メール　入力必須
	if($d['email'] === "")
	{
		$msg['err'] .= "メールアドレスを入力してください<br>";
	}
	else
	{
		if(!preg_match('/^(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*"))*@(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*\])(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*\]))*$/', $d['email']))
		{
			$msg['err'] .= "メールアドレスの形式が正しくありません<br>";
		}
	}

	//電話番号　任意
	if($d['tel'] === "")
	{

	}
	else
	{
		if(!preg_match('/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/', $d['tel']))
		{
			$msg['err'] .= "電話番号の形式が正しくありません<br>";
		}
	}

	if(empty($msg['err']))
	{
		$_SESSION['form'] = $d;

		header("location: check.php");

		exit;
	}
} 

/**
* リセットボタンを押した時
*/
else if($order === "reset")
{
	unset($_SESSION['form']);
	$d = $default_form;
} 

/**
* 他ページからの遷移の時
*/
else
{
	//確認画面からの遷移
	if(isset($_SESSION['form']))
	{
		$d = $_SESSION['form'];
	}
	//初回アクセス
	else
	{
		$d = $default_form;
	}
}


/**
* Smarty設定
*/
$smarty->assign('msg', $msg);
$smarty->assign('d', $d);



$smarty->display('form.tpl');
