<?php
/**
 * 設定ファイル
 */

/**
 * smarty設定
 */
$smarty_templete_dir = 'templates/';
$smarty_compile_dir = 'templates_c/';


/**
 * 文字コード設定
 */
mb_language("uni");
mb_internal_encoding("utf-8");
mb_http_input("auto");
mb_http_output("utf-8");


/**
 * MySQL接続設定 complete.php
 */
define('DB_NAME', 'user');
define('DB_USER', 'hanai');
define('DB_PASSWORD', 'Kazuki56');
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_CHARSET', 'utf8');
define('DB_TYPE', 'mysql');


/**
 * メール表示文の初期化
 */
$to_master = 'kazuki.hanai@fourier.jp';
$subject_customer = 'ご登録ありがとうございました';
$subject_master = '会員登録の通知';
$message_customer = 'ありがとうございました';


/**
 * メールヘッダ
 */
$headers = 'From: kazuki.hanai@fourier.jp' . "\r\n";