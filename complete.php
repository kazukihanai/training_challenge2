<?php

/**
* セッションスタート
*/
session_start();


/**
* 外部ファイル読み込み
*/
include("config.php");
require_once("libs/common.inc.php");
require_once("vender/smarty/Smarty.class.php");


/**
* 変数の初期化
*/
$d = array();
$msg = array();


/**
 * Smarty初期化
 */
$smarty = new Smarty();

$smarty->template_dir = $smarty_templete_dir;
$smarty->compile_dir = $smarty_compile_dir;


/**
* SESSION判定処理
*/
if(isset($_SESSION['form']))
{
	unset($_SESSION['form']);
}
else
{
	//入力画面へリダイレクト
	header("location: form.php");
	exit;
}


/**
 * Smarty設定
 */
$smarty->assign('d', $d);
$smarty->assign('msg', $msg);


$smarty->display('complete.tpl');
