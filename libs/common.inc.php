<?php

/**
* エスケープ処理
*/
function htmlspecailchars_array($array)
{
	foreach($_POST as $key => $value)
	{
		$value = trim($value);
		$value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
		$array[$key] = $value;
	}
	return $array;
}

/**
* 文字列整形処理
*/
function mb_convert_kana_array($array)
{
	foreach($_POST as $key => $value)
	{
		$value = mb_convert_kana($value, 'KVa');
		$array[$key] = $value;
	}
	return $array;
}

