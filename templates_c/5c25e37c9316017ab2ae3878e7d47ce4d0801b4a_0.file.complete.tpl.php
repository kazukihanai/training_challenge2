<?php
/* Smarty version 3.1.30, created on 2016-09-28 12:25:51
  from "/var/www/html/Training_challenge2/templates/complete.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57ebb6cfeb0622_34056271',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c25e37c9316017ab2ae3878e7d47ce4d0801b4a' => 
    array (
      0 => '/var/www/html/Training_challenge2/templates/complete.tpl',
      1 => 1475065229,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57ebb6cfeb0622_34056271 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<h2>お問い合わせ受付が完了しました。</h2>
		<p>ご登録いただいたメールアドレスへお問い合わせ内容を送信しましたのでご確認ください。</p>
		<p>※ メールが受信されていない場合、迷惑メールフォルダ内にないかご確認ください。</p>

		<br>

		<p><a href="form.php">入力フォームへ戻る</a></p>
	</div>
</body>
</html><?php }
}
