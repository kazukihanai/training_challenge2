<?php
/* Smarty version 3.1.30, created on 2016-09-28 12:24:58
  from "/var/www/html/Training_challenge2/templates/form.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57ebb69a46a6c8_95839543',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '565c2f9ea5b6a5676919ea1edb6cf4a319ea0da5' => 
    array (
      0 => '/var/www/html/Training_challenge2/templates/form.tpl',
      1 => 1475063464,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57ebb69a46a6c8_95839543 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<div id="header">
			<h1>お問い合わせフォーム</h1>
		</div>

		<div id="content">
			<div id="error_message">
				<?php echo $_smarty_tpl->tpl_vars['msg']->value['err'];?>

			</div>
			<form action="form.php" method="post" id="register">
				<dl>
					<dt>
						<label for="name">お名前</label> <em>必須</em>
					</dt>
					<dd>
						<input type="text" name="name" id="name" class="size-mini" placeholder="山田 太郎" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['name'];?>
">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="email">メールアドレス</label> <em>必須</em>
					</dt>
					<dd>
						<input type="text" name="email" id="email" class="size-half" placeholder="yamada@example.com" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['email'];?>
">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="tel">電話番号</label>
					</dt>
					<dd>
						<input type="text" name="tel" id="tel" class="size-half" placeholder="053-464-7708" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['tel'];?>
">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="message">メッセージ</label>
					</dt>
					<dd>
						<textarea name="message" id="message" class="size-full" rows="5" placeholder="ここにメッセージを記載してください" ><?php echo $_smarty_tpl->tpl_vars['d']->value['message'];?>
</textarea>
					</dd>
				</dl>

				<div id="buttons">
					<input id="reset_button" type="submit" name="submit[reset]" value="リセット" class="button back">
					<input type="submit" name="submit[next]" value="確認画面へ" class="button">
				</div>

			</form><!-- /form -->
		</div><!-- /content -->
	</div>
</body>
</html><?php }
}
