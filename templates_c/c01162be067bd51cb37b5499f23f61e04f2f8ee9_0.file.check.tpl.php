<?php
/* Smarty version 3.1.30, created on 2016-09-28 12:25:42
  from "/var/www/html/Training_challenge2/templates/check.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57ebb6c6ac2712_30109398',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c01162be067bd51cb37b5499f23f61e04f2f8ee9' => 
    array (
      0 => '/var/www/html/Training_challenge2/templates/check.tpl',
      1 => 1475065111,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57ebb6c6ac2712_30109398 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<h1>確認ページ</h1>
		<p>以下の内容でよろしいでしょうか</p>
		<dl>
		<dt>お名前</dt><dd><?php echo $_smarty_tpl->tpl_vars['d']->value['name'];?>
</dd> <br>
		<dt>メールアドレス</dt><dd><?php echo $_smarty_tpl->tpl_vars['d']->value['email'];?>
</dd> <br>
		<dt>電話番号</dt><dd><?php echo $_smarty_tpl->tpl_vars['d']->value['tel'];?>
</dd> <br>
		<dt>メッセージ</dt><dd><?php echo $_smarty_tpl->tpl_vars['d']->value['message'];?>
</dd> <br>
		</dl>
		<form action="check.php" method="post" id="register">
			<input id="reset_button" type="submit" name="submit[revise]" value="修正" class="button back">
			<input type="submit" name="submit[next]" value="登録" class="button">
		</form>
	</div>
</body>
</html>
<?php }
}
