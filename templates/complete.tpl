<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<h2>お問い合わせ受付が完了しました。</h2>
		<p>ご登録いただいたメールアドレスへお問い合わせ内容を送信しましたのでご確認ください。</p>
		<p>※ メールが受信されていない場合、迷惑メールフォルダ内にないかご確認ください。</p>

		<br>

		<p><a href="form.php">入力フォームへ戻る</a></p>
	</div>
</body>
</html>