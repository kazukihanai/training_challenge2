<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<h1>確認ページ</h1>
		<p>以下の内容でよろしいでしょうか</p>
		<dl>
		<dt>お名前</dt><dd>{$d.name}</dd> <br>
		<dt>メールアドレス</dt><dd>{$d.email}</dd> <br>
		<dt>電話番号</dt><dd>{$d.tel}</dd> <br>
		<dt>メッセージ</dt><dd>{$d.message}</dd> <br>
		</dl>
		<form action="check.php" method="post" id="register">
			<input id="reset_button" type="submit" name="submit[revise]" value="修正" class="button back">
			<input type="submit" name="submit[next]" value="登録" class="button">
		</form>
	</div>
</body>
</html>
