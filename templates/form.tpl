<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>お問い合わせ</title>
	<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="all">
		<div id="header">
			<h1>お問い合わせフォーム</h1>
		</div>

		<div id="content">
			<div id="error_message">
				{$msg.err}
			</div>
			<form action="form.php" method="post" id="register">
				<dl>
					<dt>
						<label for="name">お名前</label> <em>必須</em>
					</dt>
					<dd>
						<input type="text" name="name" id="name" class="size-mini" placeholder="山田 太郎" value="{$d.name}">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="email">メールアドレス</label> <em>必須</em>
					</dt>
					<dd>
						<input type="text" name="email" id="email" class="size-half" placeholder="yamada@example.com" value="{$d.email}">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="tel">電話番号</label>
					</dt>
					<dd>
						<input type="text" name="tel" id="tel" class="size-half" placeholder="053-464-7708" value="{$d.tel}">
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="message">メッセージ</label>
					</dt>
					<dd>
						<textarea name="message" id="message" class="size-full" rows="5" placeholder="ここにメッセージを記載してください" >{$d.message}</textarea>
					</dd>
				</dl>

				<div id="buttons">
					<input id="reset_button" type="submit" name="submit[reset]" value="リセット" class="button back">
					<input type="submit" name="submit[next]" value="確認画面へ" class="button">
				</div>

			</form><!-- /form -->
		</div><!-- /content -->
	</div>
</body>
</html>