<?php

/**
* セッションスタート
*/
session_start();


/**
* 外部ファイル読み込み
*/
include("config.php");
require_once("libs/common.inc.php");
require_once("vender/smarty/Smarty.class.php");


/**
* 変数の初期化
*/
$d 		= array();
$msg 	= array();
$msg['err'] = "";


/**
* Smarty初期化
*/
$smarty = new Smarty();

$smarty->template_dir = $smarty_templete_dir;
$smarty->compile_dir = $smarty_compile_dir;


/**
* SESSION判定処理
*/
if(isset($_SESSION['form']))
{
	$d = $_SESSION['form'];
}
else
{
	//入力画面へリダイレクト
	header("location: form.php");
	exit;
}


/**
* submit処理
*/
if(isset($_POST['submit']))
{
	$order = key($_POST['submit']);
}
else
{
	$order = null;
}


/**
* submitボタンの各処理
*/

//完了画面へ
if($order === "next")
{
	// メール送信
	mb_language("Japanese");
	mb_internal_encoding("UTF-8");

	/**
	 * メールの作成
	 */
	$to_customer = $d['email'];
	$name = $d['name'];
	$tel = $d['tel'];
	$message = $d['message'];

	/**
	 * 管理者送信用の本文を作成
	 */
	$message_master = $name . "\n" .
		$email . "\n" .
		$tel . "\n" .
		$message . "\n";

	if (!mb_send_mail($to_customer, $subject_customer, $message_customer, "From: {$to_master}"))
	{
		$msg['err'] .= "お客様へのメール送信に失敗しました。<br>";
	}

	if (!mb_send_mail($to_master, $subject_master, $message_master, "From: {$to_master}"))
	{
		$msg['err'] .= "管理者へのメール送信に失敗しました。<br>";
	}

	//データベースに追加
	/**
	* MySQL 接続
	*/
	$db_dsn = DB_TYPE . ':host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

	try {
		$dbh = new PDO($db_dsn, DB_USER, DB_PASSWORD);

		if ($dbh == null) {
			$msg['err'] .= "接続に失敗しました。<br>";
		} else {
			//接続に成功
		}
	} catch (PDOException $e) {
		print('Error:' . $e->getMessage());
		die();
	}


	/**
	* ユーザ追加
	*/
	$ip = $_SERVER["REMOTE_ADDR"];
	$ua = $_SERVER['HTTP_USER_AGENT'];

	$sql_add = 'insert ignore into users3 values(
		null, 
		"' . $d['name'] . '",
		"' . $d['email'] . '",
		"' . $d['tel'] . '",
		"' . $d['message'] . '",
		"' . $ip . '",
		"' . $ua . '",
		null,
		null,
		1
	)';


	$result = $dbh->query($sql_add);

	if (!$result) {
		$msg['err'] .= "クエリ実行できませんでした<br>";
		print_r($dbh->errorInfo());
		die();
		exit();
	} else {
		//クエリ成功
	}


	//Okの場合
	if(empty($msg['err']))
	{
		header("location: complete.php");
		exit;
	}


}
//修正
else if($order === "revise")
{
	header("location: form.php");
}
//その他
else
{

}


/**
 * Smarty設定
 */
$smarty->assign('d', $d);
$smarty->assign('msg', $msg);

$smarty->display('check.tpl');